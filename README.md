# SEISMAL


**SEISMAL** stands for the underground **S**torag**E** mon**I**toring u**S**ing **MA**chine **L**earning. 

<img style="float: right;" src="seismal/imgs/Logo_02.PNG" width="500">

This project was filed in order to develop a multidisciplinary partnership between scientists from:
* [Swiss Data Science Ceneter](https://datascience.ch/), SDSC (Switzerland), 
* [Geothermal Energy and Geofluids group](https://geg.ethz.ch/), ETH Zurich (Switzerland),
* [Computational Earthquake Seismology group](https://ces.kaust.edu.sa/), KAUST (Saudi Arabia), and
* [Deep Imaging group](https://dig.kaust.edu.sa/), KAUST (Saudi Arabia).

SEISMAL
[Web-page](http://seismal.mhefny.com/default.php)

### Current Status (November 2021)
The project is submitted to SDSC for scientific evaluations and potential funding.


## Goals

This project aims to leverage on advances in Machine Learning (ML), Computer Vision (CV) capability for pattern recognition, and time-lapse seismic reflection modeling for carbon dioxide (CO<sub>2</sub>) geological sequestration and utilization, in order to develop a new predictive data-driven and ML-based algorithm. Such predictive models is capable of quantifying not only the spatial and temporal changes in the properties of depleted reservoirs and deep saline aquifers (i.e., pressure and CO<sub>2</sub>-saturation), but also the ML-related uncertainty.


## Background
The use of time-lapse seismic monitoring during CO<sub>2</sub> injection is still in its infancy. To track variations in CO<sub>2</sub> saturation and pressure over time, seismic monitoring requires an understanding of the reservoir's rock and fluid thermophysical properties. Despite its growing importance in EOR operations, geologic storage, and sequestration, little is understood about CO<sub>2</sub>'s physical properties in oil-water/porous rock systems. More information is needed to remotely monitor the injected CO<sub>2</sub> and estimate the saturation distributions of injected CO<sub>2</sub> in subsurface aquifers and reservoirs.

This project aims at advancing the use of machine learning and computer vision methods in the earth and environmental sciences. In particular, the goal is to develop ML methods that enable the monitoring of gas injection into, migration through and retrieval from deep geologic formations, such as depleted oil/gas reservoirs. The ML algorithms developed in the project will be able to forecast dynamic reservoir changes and also quantify uncertainty sourcing from parametric uncertainties and also approximation uncertainties.



![Seismal_conceptual_model](seismal/imgs/Seismal_concepual_model.gif)

The major achievements of this project include:

* Investigations of new ways to compute fluid properties of oil-water-CO<sub>2</sub> mixtures.
* An algorithm to generate time-lapse seismic attribute changes as a function of changes in CO<sub>2</sub> saturation and pressure.
* New tools to invert time-lapse seismic anomalies to yield estimates of CO<sub>2</sub> saturation and pressure changes and the calibration of these tools on a synthetic dataset.
* Quantification of time-lapse seismic anomalies from different vintages of a 3-D data set from Sleipner gas field in the Norwegian North Sea.


##  Goals and benefits 

The overall scientific goal is to develop a method that enables the monitoring of gas injection into, migration through and retrieval from deep geologic formations.
There are two main components to the project: a first one that develops a joint-inversion method able to robustly predict dynamic reservoir changes directly from 4D seismic data; and a second component that accounts for uncertainty quantification arising from both parametric and approximation uncertainties.

A first objective is to develop a data-driven analytical model to quantify the sensitivity of the seismic attributes upon exposure to different fluid pairs and geological settings. A second goal is the validation and refinement of a developed ML-based algorithm for forecasting dynamic reservoir changes using the calibrated 4D seismic attributes for the fluid pairs as standard datasets.



![time_series_2D_seismic_wiggles_sleipner](seismal/imgs/time_series_2D_seismic_wiggles_sleipner.gif)

<!---<img src="seismal/imgs/time_series_2D_seismic_wiggles_sleipner.gif" width="1280"/>-->


## Approach amd methods

The general scientific approach is to use machine learning methods to forecast dynamic reservoir changes from 4D seismic data for different fluid pairs. Additionally, pattern recognition method able to detect complex seismic wave-form modifications due to gas saturations. The authors propose to use as a starting point, neural networks (cycle generative adversarial) and gradient boosting methods.

There are two main data categories to be collected in the joint-inversion method: estimated fluid-related parameters and real field 4D seismic surveys and open-hole logs. The latter are freely available for immediate usage. Time-lapse seismic survey and open-hole logs sourcing from the Sleipner CO<sub>2</sub> datasets including 2D, 3D and 4D seismic data consisting of a base model and twelve time-lapse seismic surveys as a function of CO<sub>2</sub> injections, covering a time period from January 10th, 1994 to December 10th, 2010. The field 4D seismic dataset required expert annotation
Additionally, the authors propose to use synthetic datasets obtained from seismic-wave simulations that would investigate seismic wave attenuation characteristics. These synthetic datasets are not ready yet and are to be generated in the 6-9 months of the project.


The work aims  resulted an improved and efficient ability to remotely monitor injected gas (CO<sub>2</sub>, H<sub>2</sub>) for safe storage and recovery.
